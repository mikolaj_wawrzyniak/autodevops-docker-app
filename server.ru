require 'rack'

require 'prometheus/middleware/exporter'
require 'prometheus/client'

# returns a default registry
prometheus = Prometheus::Client.registry

# create a new counter metric
http_requests = Prometheus::Client::Counter.new(:custom_counter, docstring: 'A custom counter of HTTP requests made')
# register the metric
prometheus.register(http_requests)

# start using the counter
http_requests.increment

app = Rack::Builder.new {
    map "/" do
     run lambda { |env| 
        http_requests.increment
        [200, {'Content-Type' => 'text/plain'}, ['OK']] 
     }
    end
 }

use Prometheus::Middleware::Exporter

run app