FROM ruby:2.4.1-alpine
ADD ./ /app/
WORKDIR /app
ENV PORT 5000
EXPOSE 5000

COPY . .

RUN bundle install

# CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]
CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "5000", "server.ru"]
